import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as serviceWorker from './serviceWorker'
import firebase from "firebase/app"

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyANI0sqP3fNtGqYsARyawryLUwXJXpSPVw",
    authDomain: "locateme-9ef81.firebaseapp.com",
    databaseURL: "https://locateme-9ef81.firebaseio.com",
    projectId: "locateme-9ef81",
    storageBucket: "",
    messagingSenderId: "288316847570",
    appId: "1:288316847570:web:78507fb78f70f77518440b"
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

ReactDOM.render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
