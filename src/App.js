import React, { useEffect, useState } from 'react'
import format from "dateformat"
import firebase from "firebase/app"
import "firebase/database"
import loadjs from 'loadjs'

// import style from "./style.json"

// global variable
var lines = [],
  markers = [],
  pointsDone = [],
  polylines = [],
  stepSize = 300, //Size of every animation increment
  stepTime = 5;   //Decrease to speed up


const App = () => {

  const [email, setEmail] = useState("")

  const [result, setResult] = useState(localStorage.getItem('email'))

  const [values, setValues] = useState([])

  const [load, setLoad] = useState(true)

  const [toggle, setToggle] = useState(true)

  const [map, setMap] = useState(null)

  function parseDataByDate(arr) {

    let result = {}

    for (let i in arr) {

      const date = format(new Date(arr[i].timestamp), "dd-mmmm-yyyy")

      if (!result[date]) {

        result[date] = {}

        result[date].locate = []

      }

      const latlng = arr[i].lat + arr[i].lng

      if (!result[date][latlng.toString()]) {

        result[date][latlng.toString()] = "required"

        result[date].timestamp = date

        result[date].locate.push({
          lat: arr[i].lat,
          lng: arr[i].lng,
          time: new Date(arr[i].timestamp)
        })
      }

    }

    return result

  }

  // the smooth zoom function
  function smoothZoom(map, max, cnt) {

    if (cnt >= max) {

      return;
    }

    else {

      let z = window.google.maps.event.addListener(map, 'zoom_changed', function (event) {

        window.google.maps.event.removeListener(z);

        smoothZoom(map, max, cnt + 1);

      });

      setTimeout(function () { map.setZoom(cnt) }, 80); // 80ms is what I found to work well on my system -- it might not work well on all systems

    }
  }

  useEffect(() => {

    const el = document.querySelector('#map-google')

    if (!map || !result) setMap(new window.google.maps.Map(el, {
      center: { lat: 0.6432589, lng: 108.9829439 },
      zoom: 5,
      // styles: style
    }))

    if (!result) return

    (async () => {

      setLoad(false)

      const snap = await firebase.database().ref("data").orderByChild("email").equalTo(result).once("value")

      if (!snap.exists()) {

        alert("Data tidak temukan")

        setEmail("")

        setResult(null)

        setValues([])

        setLoad(true)

        localStorage.removeItem('email')

        return setResult(null)

      }

      let snapshot = Object.values(snap.val()).reverse()

      setValues(Object.values(parseDataByDate(snapshot)))

      setLoad(true)

      loadjs([
        '/assets/js/jquery-2.2.0.min.js',
        '/assets/js/timedropper.js',
        '/assets/js/custom.js'
      ], { async: false })

    })()

  }, [result])



  // Removes the markers from the map, but keeps them in the array.
  function clearMarkers() {

    for (let i in markers) {

      markers[i].marker.setMap(null)

      markers[i].marker.setVisible(false)

      markers[i].infowindow = null
    }

    for (let i in polylines) {

      polylines[i].setMap(null)

    }

    lines = []

    pointsDone = []

    markers = []

  }

  async function choseDate(data) {

    // e.preventDefault()

    toogleNavigation()

    clearMarkers()

    data.locate = data.locate.sort((a, b) => a.time - b.time)

    const center = new window.google.maps.LatLng(data.locate[0].lat, data.locate[0].lng)

    map.panTo(center)

    smoothZoom(map, 18, map.getZoom());

    const icon = {
      url: "/assets/img/marker.png",
      scaledSize: new window.google.maps.Size(50, 50)
    }

    for (let i in data.locate) {

      const time = format(data.locate[i].time, "HH:MM")

      const address = data.locate[i].address || 'Jalan tidak ditemukan'

      const contentString = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<div id="bodyContent">' +
        '' + address + '<br/> jam <b>' + time + '</b>' +
        '</div>' +
        '</div>';


      var infowindow = new window.google.maps.InfoWindow({
        content: contentString
      })

      const marker = new window.google.maps.Marker({
        position: new window.google.maps.LatLng(data.locate[i].lat, data.locate[i].lng),
        map: map,
        icon,
        visible: false
        // animation: window.google.maps.Animation.DROP, //animation
      })

      lines.push(new window.google.maps.LatLng(data.locate[i].lat, data.locate[i].lng))

      markers.push({ marker, infowindow })

    }

    pointsDone.push(lines[0])

    //Start the animation on first point in mapPoints
    pathAnimation(0)

    //Following function gets called one polyline at a time
    function pathAnimation(point) {

      //Make marker visible once path has reached that point
      markers[point].marker.setVisible(true)

      if (markers[point - 1]) markers[point - 1].infowindow.close(map, markers[point].marker)

      markers[point].infowindow.open(map, markers[point].marker)

      window.google.maps.event.addListener(markers[point].marker, 'click', () => {

        markers[point].infowindow.open(map, markers[point].marker)

      })

      //Set start & end point for this polyline
      const startPoint = pointsDone[point]

      const endPoint = lines[point + 1] ? lines[point + 1] : lines[point]

      map.panTo(endPoint)

      //Create the polyline
      const myPath = new window.google.maps.Polyline({
        path: pointsDone,
        geodesic: true,
        strokeColor: '#0BB7A5',
        strokeOpacity: 1.0,
        strokeWeight: 5,
        map
      })

      polylines.push(myPath)

      //Add this point to the array keeping points that have already been animated to
      if (endPoint) pointsDone.push(endPoint);

      var step = 0

      //Animation loop
      const myInterval = setInterval(() => {

        try {

          step += 1;

          if (step > stepSize) {

            //Done drawing, clear the interval, and call pathAnimation() again, IF we're not done animating all the polylines
            clearInterval(myInterval);

            if (pointsDone.length - 1 < lines.length) {

              pathAnimation(pointsDone.length - 1);

            }

          } else {

            //Not done drawing yet...
            const progress = window.google.maps.geometry.spherical.interpolate(startPoint, endPoint, step / stepSize);

            myPath.setPath([startPoint, progress])

          }

        } catch (error) {

          //Done drawing, clear the interval, and call pathAnimation() again, IF we're not done animating all the polylines
          clearInterval(myInterval);

        }

      }, stepTime)

    }

  }

  function convertToDate(date, time) {

    const startTime = new Date(date)

    const parts = time.match(/(\d+):(\d+) (am|pm)/)

    if (parts) {

      let hours = parseInt(parts[1])

      const minutes = parseInt(parts[2])

      const tt = parts[3]

      if (tt === 'pm' && hours < 12) hours += 12

      startTime.setHours(hours, minutes, 0, 0)

    }

    return startTime

  }

  function filterByTime(data, e) {

    e.preventDefault()

    const dari = convertToDate(data.timestamp, e.target.elements.dari.value)

    const ke = convertToDate(data.timestamp, e.target.elements.ke.value)

    const result = data.locate.filter(i => Date.parse(i.time) >= Date.parse(dari) && Date.parse(i.time) <= Date.parse(ke))

    if (result[0])
      choseDate({ ...data, locate: result })

  }

  function submit(e) {

    e.preventDefault()

    setResult(email)

    localStorage.setItem('email', email)

  }

  function changeEmail() {

    setEmail("")

    setResult(null)

    setValues([])

    localStorage.removeItem('email')

  }

  function toogleNavigation() {

    const wrapper = document.querySelector('.map-filter-wrapper')

    if (toggle) {

      wrapper.setAttribute('style', 'opacity : 100; width: 100%')

      setToggle(!toggle)

    } else {

      wrapper.style = {
        opacity: "0",
        width: "0"
      }

      setToggle(!toggle)

    }
  }

  function accordion(e) {

    const listDate = document.querySelector('.list-date')

    if (listDate) listDate.className = ""

    e.target.className = "list-date"

    const allEl = document.querySelectorAll('.accordion')

    allEl.forEach(i => i.style.display = 'none')

    const el = e.target.nextElementSibling

    el.style.display = el.style ? el.style.display === 'none' ? 'block' : 'none' : 'none'

  }

  return (
    <div className="page-wrapper">

      <div className="main-wrapper">
        <div className="main">
          <div className="main-inner">
            <div className="content">

              <div className="map-google-wrapper pull-top">

                <div style={{ height: "100vh", overflowY: "hidden" }} id="map-google"></div>


                <button onClick={toogleNavigation} className="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target=".nav-primary-wrapper">
                  <span></span>
                  <span></span>
                  <span></span>
                </button>

                <div className="map-filter-wrapper">

                  <div hidden={load} className="map-filter filter">
                    <center>
                      <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
                    </center>
                  </div>

                  <div hidden={!load} style={{ height: values.length < 1 ? "fit-content" : null, overflowY: "auto", overflowX: "hidden" }} className="map-filter filter">

                    {
                      values.length < 1
                        ? (
                          <form onSubmit={submit}>
                            <div className="form-group">
                              <input
                                type="email"
                                className="form-control"
                                placeholder="Masukkan email lalu enter"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                              />
                            </div>
                          </form>
                        ) : (
                          <span
                            className="listing-box-image-links"
                          >
                            {
                              values.map((i, x) => (
                                <div style={{ borderBottom: "1px solid white" }} key={x}>
                                  <a
                                    href="#!"
                                    // onClick={choseDate.bind(this, i)}
                                    onClick={accordion}
                                    style={{ textDecoration: "none" }}
                                  >
                                    <i
                                      style={{ fontSize: "20px" }}
                                      className="fa fa-street-view"
                                    />
                                    {i.timestamp.replace(/-/g, " ")}
                                  </a>
                                  <div className="row accordion" style={{ padding: "10px 20px", display: "none" }}>
                                    <form onSubmit={filterByTime.bind(this, i)}>
                                      <div style={{ padding: 0 }} className="col-md-5 col-xs-5">
                                        <input style={{ borderRadius: 0, textAlign: "center" }} type="text" className="form-control timepicker" name="dari" />
                                      </div>
                                      <div style={{ padding: 0 }} className="col-md-5 col-xs-5">
                                        <input style={{ borderRadius: 0, textAlign: "center" }} type="text" className="form-control timepicker" name="ke" />
                                      </div>
                                      <div style={{ padding: 0 }} className="col-md-2 col-xs-2">
                                        <button
                                          style={{ borderRadius: 0, paddingLeft: "14px" }}
                                          className="btn btn-primary btn-block"
                                        >
                                          <i
                                            style={{ fontSize: "20px" }}
                                            className="fa fa-filter"
                                          />
                                        </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              ))
                            }
                          </span>
                        )
                    }

                  </div>

                  {
                    values.length > 0 ? (
                      <div
                        className="map-filter filter"
                        style={{
                          marginTop: "5px",
                          padding: "0"
                        }}
                      >
                        <button
                          type="button"
                          className="btn btn-primary btn-block"
                          onClick={changeEmail}
                        >
                          Ganti Email
                        </button>
                      </div>
                    ) : null
                  }

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

    </div >
  );
}

export default App;
