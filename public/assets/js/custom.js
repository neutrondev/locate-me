$('.timepicker').timeDropper({
    setCurrentTime: false,
    meridians: true,
    primaryColor: "#0BB7A5",
    borderColor: "#0BB7A5",
    minutesInterval: '15',
})